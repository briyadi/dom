let h1 = document.createElement('h1')
h1.textContent = 'Aplikasi merubah warna'
document.body.append(h1)

let labelBackground = document.createElement('label')
labelBackground.textContent = 'Warna latar'

let inputBackground = document.createElement('input')
inputBackground.type = 'color'

let br = document.createElement('br')

let labelColor = document.createElement('label')
labelColor.textContent = 'Warna text'

let inputColor = document.createElement('input')
inputColor.type = 'color'

document.body.append(labelBackground)
document.body.append(inputBackground)
document.body.append(br)
document.body.append(labelColor)
document.body.append(inputColor)

inputBackground.addEventListener('change', function(e) {
    document.body.style.backgroundColor = e.target.value
})

inputColor.addEventListener('change', function(e) {
    document.body.style.color = e.target.value
})