// create new navbar
let navbar = document.createElement('div')
document.body.append(navbar)

navbar.classList.add('navbar', 'navbar-dark', 'bg-primary')

// create content inside navbar
let navbarBrand = document.createElement('div')
navbar.append(navbarBrand)

navbarBrand.classList.add('navbar-brand')
navbarBrand.innerText = 'Navbar'

// create new container ...
let container = document.createElement('div')
document.body.append(container)

container.classList.add('container', 'mt-4')

// create todo input ...
let input = document.createElement('input')
container.append(input)

input.classList.add('form-control')
input.type = 'text'
input.placeholder = 'Input todo here...'

let toDo = document.createElement('div')
container.append(toDo)
toDo.id = "toDo"

// Todo list...
var todos = ['Belajar JavaScript', 'Belajar CSS', 'Belajar HTML']

function loadTodos() {
    toDo.innerHTML = ''
    for (let i in todos) {
        toDo.innerHTML += `
        <div class="card mt-2">
            <div class="card-body">
                <button class="btn btn-danger btn-sm btn-remove">&times;</button>
                ${todos[i]}
            </div>
        </div>
        `
    }
}

loadTodos()

input.addEventListener('keypress', function(event) {
    if (event.key == 'Enter') {
        // tambahkan 1 todo baru di paling awal
        todos.unshift(input.value)
        // load todo lagi
        loadTodos()
        // kosongkan input
        input.value = ''
    }
})

document.addEventListener('click', function(event) {
    if (event.target.classList.contains('btn-remove')) {
        event.target.parentNode.parentNode.remove()
    }
})