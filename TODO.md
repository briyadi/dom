1. Buatalah sebuah file dengan extensi .html
2. isi file tersebut dengan dokumen html
3. Buatalah sebuah file dengan extensi .js
4. masukan file tersebut kedalam file html yang sudah dibuat, 
   caranya dengan menambahkan tag `script` dengan attribute `src="namafile.js"`
5. copy CDN bootstrap ke dalam html di tag `head`
6. buatlah sebuah elemen html `div` dan masukkan kedalam body menggunakan **DOM** `document.createElement('div')`
   tambahkan attribute `class` pada `div` tersebut. isi attribute class tersebut dengan `navbar navbar-dark bg-primary`. kamu bisa menggunakan `classList.add(nama classnya)`.
7. buatlah sebuah elemen html `div` dan masukkan kedalam `div` navbar yang sudah dibuat tadi. beri class `navbar-brand` pada `div` yang baru saja dibuat, kemudian isi `div` tersebut dengan text **Navbar**
8. buatlah sebuah elemen html `div` dan masukkan kedalam `body`. beri class `container` dan `mt-4` pada `div` tersebut
9. buatlah sebuah elemen html `input` dan masukkan ke dalam container. beri class pada input tersebut `form-control`, beri attribute `type` pada input tersebut yaitu `text`, beri attribute placeholder `Input todo here...`.
10. buatlah sebuah elemen html `div` dan masukkan kedalam container. beri id `todo` pada div tersebut.
11. buatlah sebuah variable dengan nama `todos` yang isinya array dari kumpulan string.
12. buatlah sebuah function `loadTodos` untuk menampilkan seluruh `todos` kedalam id `todo`. buatlah perulangan pada function tersebut sejumlah isi dari variable `todos`. di dalam setiap perulangannya, gunakan innerHTML untuk menambah content ke dalam id `todo`. exekusi function tersebut.
13. buatlah satu event listener on keypress / key down pada elemen input, yang ketika `keyCode == 13` atau `key == 'Enter'`, tambahkan 1 elemen array pada variable `todos`. kemudian eksekusi `loadTodos` lagi di dalam listener tersebut dan kosongkan kembali value elemen `input`.
14. Tambahkan button remove di setiap todonya...